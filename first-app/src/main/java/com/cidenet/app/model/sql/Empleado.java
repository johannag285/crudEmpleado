package com.cidenet.app.model.sql;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="empleado")
public class Empleado {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String otroNombre;
	private String pais;
	private String tipoIdentificacion;
	private String numeroIdentificacion;
	private String correoElectronico;
	private Date fechaIngreso;
	private String areaEmpleado;
	private String estado;
	private Date fechaRegistro;
	private Date fechaEdicion;
}
