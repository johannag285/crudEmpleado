package com.cidenet.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.app.model.sql.Empleado;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Integer>{
	Empleado findByNumeroIdentificacion(String numeroIdentificacion);
	List<Empleado> findByCorreoElectronicoStartsWith(String correo);
	List<Empleado> findByNumeroIdentificacionStartsWith(String numeroIdentificacion);
	List<Empleado> findByTipoIdentificacionStartsWith(String tipoIdentificacion);
	List<Empleado> findByPrimerNombreStartsWith(String primerNombre);
	List<Empleado> findBySegundoApellidoStartsWith(String segundoApellido);
	List<Empleado> findByOtroNombreStartsWith(String otroNombre);
	List<Empleado> findByPaisStartsWith(String pais);
	List<Empleado> findByEstadoStartsWith(String estado);
	List<Empleado> findByPrimerApellidoStartsWith(String estado);
}
