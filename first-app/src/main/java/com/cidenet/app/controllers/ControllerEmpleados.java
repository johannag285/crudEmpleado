package com.cidenet.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.app.dto.EmpleadoDTO;
import com.cidenet.app.dto.EmpleadoRequestDTO;
import com.cidenet.app.services.ConsultarEmpleadoServiceImp;
import com.cidenet.app.services.RegistroEmpleadoServiceImp;

@RestController
@RequestMapping("/ws/empleado")
public class ControllerEmpleados {
	
	private RegistroEmpleadoServiceImp registroEmpleado;
	private ConsultarEmpleadoServiceImp consultarEmpleadoServiceImp;

	@Autowired
	public ControllerEmpleados(RegistroEmpleadoServiceImp registroEmpleado,
			ConsultarEmpleadoServiceImp consultarEmpleadoServiceImp) {
		this.registroEmpleado = registroEmpleado;
		this.consultarEmpleadoServiceImp = consultarEmpleadoServiceImp;
	}
	
	@PostMapping("/registrarEmpelado")
	public String registrarEmpelado(@RequestBody EmpleadoDTO empleadoRequest) {
		return registroEmpleado.add(empleadoRequest);
	}
	
	@GetMapping("/findUsersByNumeroIdentificacion")
	public List<EmpleadoDTO> findUsersByNumeroIdentificacion(String numeroIdentificacion) {
		return consultarEmpleadoServiceImp.findUsersByNumeroIdentificacion(numeroIdentificacion);
	}
	
	@GetMapping("/findByTipoIdentificacion")
	public List<EmpleadoDTO> findByTipoIdentificacion(String tipoIdentificacion) {
		return consultarEmpleadoServiceImp.findUsersByTipoIdentificacion(tipoIdentificacion);
	}
	
	@GetMapping("/findByPrimerNombre")
	public List<EmpleadoDTO> findByPrimerNombre(String primerNombre) {
		return consultarEmpleadoServiceImp.findByPrimerNombre(primerNombre);
	}
	
	@GetMapping("/findBySegundoApellido")
	public List<EmpleadoDTO> findBySegundoApellido(String segundoApellido) {
		return consultarEmpleadoServiceImp.findBySegundoApellido(segundoApellido);
	}
	
	@GetMapping("/findByCorreoElectronico")
	public List<EmpleadoDTO> findByCorreoElectronico(String correo) {
		return consultarEmpleadoServiceImp.findByCorreoElectronico(correo);
	}
	
	@GetMapping("/findByOtroNombre")
	public List<EmpleadoDTO> findByOtroNombre(String otroNombre) {
		return consultarEmpleadoServiceImp.findByOtroNombre(otroNombre);
	}
	
	@GetMapping("/findByPais")
	public List<EmpleadoDTO> findByPais(String pais) {
		return consultarEmpleadoServiceImp.findByPais(pais);
	}
	
	@GetMapping("/findByEstado")
	public List<EmpleadoDTO> findByEstado(String estado) {
		return consultarEmpleadoServiceImp.findByEstado(estado);
	}
	
	@GetMapping("/findByPrimerApellido")
	public List<EmpleadoDTO> findByPrimerApellido(String primerApellido) {
		return consultarEmpleadoServiceImp.findByPrimerApellido(primerApellido);
	}
	
	@PutMapping
	public EmpleadoDTO modify(@RequestBody EmpleadoRequestDTO empleadoRequestDTO) {
		return consultarEmpleadoServiceImp.modify(empleadoRequestDTO);
	}
	
	@DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
		consultarEmpleadoServiceImp.delete(id);
    	return ResponseEntity.ok().build();
    }
}
