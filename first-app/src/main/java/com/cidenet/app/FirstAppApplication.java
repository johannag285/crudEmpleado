package com.cidenet.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class FirstAppApplication implements CommandLineRunner {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(FirstAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*String sql = "INSERT INTO empleado (primerApellido, segundoApellido, primerNombre, otroNombre, pais, "
				+ "tipoIdentificacion, numeroIdentificacion, correoElectronico, fechaIngreso, "
				+ "areaEmpleado, estado, fechaRegistro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		int result = jdbcTemplate.update(sql, "Gonzalez", "Artunduaga", "Johanna", "", "COL", "cc", "1012458",
				"johanna@out.com", null, "1", "activo", null);
		
		if(result > 0) {
			System.out.println("DATOS INSERTADOS...");
		}*/

	}

}
