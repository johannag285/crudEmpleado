package com.cidenet.app.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmpleadoDTO implements Serializable{
	private static final long serialVersionUID = 4907229684810992637L;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String otroNombre;
	private String pais;
	private String tipoIdentificacion;
	private String numeroIdentificacion;
	private String correoElectronico;
	private String fechaIngreso;
	private String areaEmpleado;
	private String estado;
	private String fechaRegistro;
	private String fechaEdicion;
}
