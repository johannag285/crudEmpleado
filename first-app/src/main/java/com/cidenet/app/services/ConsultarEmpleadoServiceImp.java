package com.cidenet.app.services;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cidenet.app.dto.EmpleadoDTO;
import com.cidenet.app.dto.EmpleadoRequestDTO;

@Service
public class ConsultarEmpleadoServiceImp {
	ModelMapper modelMapper;
	EmpleadoEntityServiceImp empleadoEntityImp;
	
	@Autowired
	public ConsultarEmpleadoServiceImp(EmpleadoEntityServiceImp empleadoEntityImp, ModelMapper modelMapper) {
		this.empleadoEntityImp = empleadoEntityImp;
		this.modelMapper = modelMapper;
	}
	
	
	public List<EmpleadoDTO> findUsersByNumeroIdentificacion(String numeroIdentificacion) {
		return empleadoEntityImp.findByNumeroIdentificacionStartsWith(numeroIdentificacion);
	}
	
	public List<EmpleadoDTO> findUsersByTipoIdentificacion(String tipoIdentificacion) {
		return empleadoEntityImp.findByTipoIdentificacion(tipoIdentificacion);
	}
	
	public List<EmpleadoDTO> findByPrimerNombre(String primerNombre) {
		return empleadoEntityImp.findByPrimerNombre(primerNombre);
	}
	
	public List<EmpleadoDTO> findBySegundoApellido(String segundoApellido) {
		return empleadoEntityImp.findBySegundoApellido(segundoApellido);
	}
	
	public List<EmpleadoDTO> findByCorreoElectronico(String correo) {
		return empleadoEntityImp.findByCorreoElectronico(correo);
	}
	
	public List<EmpleadoDTO> findByOtroNombre(String otroNombre) {
		return empleadoEntityImp.findByOtroNombre(otroNombre);
	}
	
	public List<EmpleadoDTO> findByPais(String pais) {
		return empleadoEntityImp.findByPais(pais);
	}
	
	public List<EmpleadoDTO> findByEstado(String estado) {
		return empleadoEntityImp.findByEstado(estado);
	}
	
	public List<EmpleadoDTO> findByPrimerApellido(String primerApellido) {
		return empleadoEntityImp.findByPrimerApellido(primerApellido);
	}
	
	public EmpleadoDTO modify(EmpleadoRequestDTO empleadoRequestDTO) {
		return empleadoEntityImp.modify(empleadoRequestDTO);
	}
	
	public void delete(int id) {
		 empleadoEntityImp.delete(id);
	}
}
