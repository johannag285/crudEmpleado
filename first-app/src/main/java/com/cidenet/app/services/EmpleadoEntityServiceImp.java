package com.cidenet.app.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cidenet.app.dto.EmpleadoDTO;
import com.cidenet.app.dto.EmpleadoRequestDTO;
import com.cidenet.app.model.sql.Empleado;
import com.cidenet.app.repositories.EmpleadoRepository;

@Service
public class EmpleadoEntityServiceImp {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoEntityServiceImp.class);
	private ModelMapper modelMapper;
	private EmpleadoRepository empleadoRepository;
	private DateFormat dFormat;
	private DateFormat dFormat1;
	private SimpleDateFormat dateFormat;

	@Autowired
	public EmpleadoEntityServiceImp(ModelMapper modelMapper, EmpleadoRepository empleadoRepository) {
		this.modelMapper = modelMapper;
		this.empleadoRepository = empleadoRepository;
		this.dFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		this.dFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
		this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	}

	public EmpleadoDTO add(EmpleadoDTO empleadoDTO) {
		LOGGER.info("Añadiendo empleado: " + empleadoDTO);
		
		Date date2 = new Date();
		
		Empleado empleado = new Empleado();
		try {
			
			dFormat.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
			Date date1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(dFormat.format(date2));  
			empleado.setFechaIngreso(date1);
			
			
		    Date s = dateFormat.parse(empleadoDTO.getFechaRegistro());
		    Date date3 = new SimpleDateFormat("dd-MMM-yyyy").parse(dFormat1.format(s)); 
		    empleado.setFechaRegistro(date3);
		    
			empleado.setAreaEmpleado(empleadoDTO.getAreaEmpleado());
			empleado.setCorreoElectronico(empleadoDTO.getCorreoElectronico());
			empleado.setEstado(empleadoDTO.getEstado());
			empleado.setNumeroIdentificacion(empleadoDTO.getNumeroIdentificacion());
			empleado.setOtroNombre(empleadoDTO.getOtroNombre());
			empleado.setPais(empleadoDTO.getPais());
			empleado.setPrimerApellido(empleadoDTO.getPrimerApellido());
			empleado.setPrimerNombre(empleadoDTO.getPrimerNombre());
			empleado.setSegundoApellido(empleadoDTO.getSegundoApellido());
			empleado.setTipoIdentificacion(empleadoDTO.getTipoIdentificacion());
			empleado.setFechaEdicion(null);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		empleadoRepository.save(empleado);
		LOGGER.info("Empleado añadido satisfactoriamente");
		return modelMapper.map(empleado, EmpleadoDTO.class);
	}

	public EmpleadoDTO findByNumeroIdentificacion(String numeroIdentificacion) {
		Empleado empleado = empleadoRepository.findByNumeroIdentificacion(numeroIdentificacion);
		if (empleado != null)
			return modelMapper.map(empleado, EmpleadoDTO.class);
		else
			return null;
	}
	
	public List<EmpleadoDTO> findByCorreoElectronico(String correo) {
		List<Empleado> listEmpleado = empleadoRepository. findByCorreoElectronicoStartsWith(correo);
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findByNumeroIdentificacionStartsWith(String numeroIdentificacion) {
		List<Empleado> listEmpleado = empleadoRepository. findByNumeroIdentificacionStartsWith(numeroIdentificacion);
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findByTipoIdentificacion(String tipoIdentificacion) {
		if(tipoIdentificacion.equalsIgnoreCase("Cédula de Ciudadanía") || tipoIdentificacion.equalsIgnoreCase("Cedula de Ciudadania"))
			tipoIdentificacion = "cc"; 
		else if(tipoIdentificacion.equalsIgnoreCase("Cédula de Extranjería") || tipoIdentificacion.equalsIgnoreCase("Cedula de Extranjeria"))
			tipoIdentificacion = "ce"; 
		else if(tipoIdentificacion.equalsIgnoreCase("Pasaporte"))
			tipoIdentificacion = "pp"; 
		else if(tipoIdentificacion.equalsIgnoreCase("Permiso Especial"))
			tipoIdentificacion = "pe"; 
		
		List<Empleado> listEmpleado = empleadoRepository. findByTipoIdentificacionStartsWith(tipoIdentificacion);
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	
	public List<EmpleadoDTO> findByPrimerNombre(String primerNombre) {
		List<Empleado> listEmpleado = empleadoRepository. findByPrimerNombreStartsWith(primerNombre.toUpperCase());
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findBySegundoApellido(String segundoApellido) {
		List<Empleado> listEmpleado = empleadoRepository. findBySegundoApellidoStartsWith(segundoApellido.toUpperCase());
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findByOtroNombre(String otroNombre) {
		List<Empleado> listEmpleado = empleadoRepository.findByOtroNombreStartsWith(otroNombre.toUpperCase());
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findByPais(String pais) {
		List<Empleado> listEmpleado = empleadoRepository.findByPaisStartsWith(pais);
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findByEstado(String estado) {
		List<Empleado> listEmpleado = empleadoRepository.findByEstadoStartsWith(estado);
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public List<EmpleadoDTO> findByPrimerApellido(String primerApellido) {
		List<Empleado> listEmpleado = empleadoRepository.findByPrimerApellidoStartsWith(primerApellido.toUpperCase());
		if (listEmpleado != null) {
			List<EmpleadoDTO> listEmpleadoDTO = new ArrayList<EmpleadoDTO>();
			for(Empleado empleado : listEmpleado) {
				EmpleadoDTO empleadoDTO = modelMapper.map(empleado, EmpleadoDTO.class);
				listEmpleadoDTO.add(empleadoDTO);
			}
			return listEmpleadoDTO ;
		}else
			return null;
	}
	
	public EmpleadoDTO modify(EmpleadoRequestDTO empleadoRequestDTO){
		Empleado empleado = empleadoRepository.findById(Integer.valueOf(empleadoRequestDTO.getId())).orElse(null);
		Empleado tempEmpleado = empleado;
		if(empleado == null)
			return null;
		
		if(!tempEmpleado.getPrimerApellido().equals(empleadoRequestDTO.getPrimerApellido().toUpperCase())
				|| !tempEmpleado.getPrimerNombre().equals(empleadoRequestDTO.getPrimerNombre().toUpperCase())) {
			String correoElectronico = "";
			String dominio = "";
			String [] correoSplit = tempEmpleado.getCorreoElectronico().split("@");
			if(empleadoRequestDTO.getPais().equalsIgnoreCase("Colombia"))
				dominio = "cidenet.com.co";
			else if (empleadoRequestDTO.getPais().equalsIgnoreCase("Estados Unidos")) 
				dominio = "cidenet.com.us";
			
			if(correoSplit[0].split("\\.").length == 3) {
				correoElectronico = empleadoRequestDTO.getPrimerNombre() + "." +empleadoRequestDTO.getPrimerApellido() + "." + correoSplit[0].split("\\.")[2] + "@" + dominio;
			}else {
				correoElectronico = empleadoRequestDTO.getPrimerNombre() + "." +empleadoRequestDTO.getPrimerApellido() + "@" + dominio;
			}
			empleadoRequestDTO.setCorreoElectronico(correoElectronico);
		}
		
		empleado.setPrimerApellido(empleadoRequestDTO.getPrimerApellido() != null 
				&& !empleadoRequestDTO.getPrimerApellido().isEmpty() 
				? empleadoRequestDTO.getPrimerApellido(): tempEmpleado.getPrimerApellido());
		
		empleado.setEstado(empleadoRequestDTO.getEstado() != null  
				&& !empleadoRequestDTO.getEstado().isEmpty()? empleadoRequestDTO.getEstado(): tempEmpleado.getEstado());
		
		empleado.setNumeroIdentificacion(empleadoRequestDTO.getNumeroIdentificacion() != null 
				&& !empleadoRequestDTO.getNumeroIdentificacion().isEmpty() ? empleadoRequestDTO.getNumeroIdentificacion() : tempEmpleado.getNumeroIdentificacion() );
		
		
		empleado.setOtroNombre(empleadoRequestDTO.getOtroNombre() != null 
				&& !empleadoRequestDTO.getOtroNombre().isEmpty()? empleadoRequestDTO.getOtroNombre() : tempEmpleado.getOtroNombre());
		
		empleado.setPais(empleadoRequestDTO.getPais() != null 
				&& !empleadoRequestDTO.getPais().isEmpty()? empleadoRequestDTO.getPais(): tempEmpleado.getPais());
		
		empleado.setPrimerNombre(empleadoRequestDTO.getPrimerNombre() != null 
				&& !empleadoRequestDTO.getPrimerNombre().isEmpty()? empleadoRequestDTO.getPrimerNombre(): tempEmpleado.getPrimerNombre());
		
		empleado.setSegundoApellido(empleadoRequestDTO.getSegundoApellido() != null 
				&& !empleadoRequestDTO.getSegundoApellido().isEmpty()? empleadoRequestDTO.getSegundoApellido() : tempEmpleado.getSegundoApellido());
		
		empleado.setTipoIdentificacion(empleadoRequestDTO.getTipoIdentificacion() != null
				&& !empleadoRequestDTO.getTipoIdentificacion().isEmpty()?  empleadoRequestDTO.getTipoIdentificacion():tempEmpleado.getTipoIdentificacion());
		
		Date date2 = new Date();
		dFormat.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
		Date date1;
		try {
			date1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(dFormat.format(date2));
			empleado.setFechaEdicion(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		
		empleadoRepository.save(empleado);
		empleadoRequestDTO.setFechaEdicion(empleado.getFechaEdicion().toString());
		empleadoRequestDTO.setEstado(empleado.getEstado());
		empleadoRequestDTO.setFechaRegistro(empleado.getFechaRegistro().toString());
		empleadoRequestDTO.setFechaIngreso(empleado.getFechaIngreso().toString());
		return modelMapper.map(empleadoRequestDTO, EmpleadoDTO.class);
	}
	
	public void delete(int id) {
		empleadoRepository.deleteById(Integer.valueOf(id));
	}
}
