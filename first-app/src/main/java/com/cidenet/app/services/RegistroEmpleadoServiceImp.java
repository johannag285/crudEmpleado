package com.cidenet.app.services;

import java.text.Normalizer;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cidenet.app.dto.EmpleadoDTO;

@Service
public class RegistroEmpleadoServiceImp {
	ModelMapper modelMapper;
	EmpleadoEntityServiceImp empleadoEntityImp;
	
	@Autowired
	public RegistroEmpleadoServiceImp(EmpleadoEntityServiceImp empleadoEntityImp, ModelMapper modelMapper) {
		this.empleadoEntityImp = empleadoEntityImp;
		this.modelMapper = modelMapper;
	}
	
	public String add(EmpleadoDTO empleadoDTO) {
		String response = "";
		response += validarNombre(empleadoDTO.getPrimerApellido().toUpperCase(), "primer apellido", 20);
		response += validarNombre(empleadoDTO.getSegundoApellido().toUpperCase(), "segundo apellido", 20);
		response += validarNombre(empleadoDTO.getPrimerNombre().toUpperCase(), "primer nombre", 20);
		response += validarNombre(empleadoDTO.getOtroNombre().toUpperCase(), "otro nombre", 50);
		if(!response.isEmpty()) {
			return response;
		}
		

		String primerApellido = quitarTildes(empleadoDTO.getPrimerApellido()).toUpperCase(); 
		empleadoDTO.setPrimerApellido(primerApellido);

		String segundoApellido = quitarTildes(empleadoDTO.getSegundoApellido()).toUpperCase(); 
		empleadoDTO.setSegundoApellido(segundoApellido);
		
		String primerNombre = quitarTildes(empleadoDTO.getPrimerNombre()).toUpperCase(); 
		empleadoDTO.setPrimerNombre(primerNombre);
		
		String otroNombre = quitarTildes(empleadoDTO.getOtroNombre()).toUpperCase(); 
		empleadoDTO.setOtroNombre(otroNombre);
		
		if(!empleadoDTO.getPais().equalsIgnoreCase("Colombia") 
				|| !empleadoDTO.getPais().equalsIgnoreCase("Estados Unidos")) {
			response+= "Debe ingresar un país válido";
		}
		
		if(!empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Cédula de Ciudadanía") 
				|| !empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Cédula de Extranjería")
				|| !empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Pasaporte")
				|| !empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Permiso Especial")) {
			response+= "Debe ingresar un tipo de identificacion válido";
		}
		
		if(empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Cédula de Ciudadanía"))
			empleadoDTO.setTipoIdentificacion("cc");
		else if(empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Cédula de Extranjería"))
			empleadoDTO.setTipoIdentificacion("ce");
		else if(empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Pasaporte"))
			empleadoDTO.setTipoIdentificacion("pp");
		else if(empleadoDTO.getTipoIdentificacion().equalsIgnoreCase("Permiso Especial"))
			empleadoDTO.setTipoIdentificacion("pe");
		
		
		Pattern  pattern = Pattern.compile("^[a-zA-Z0-9]+$");
		Matcher matcher = pattern.matcher(empleadoDTO.getNumeroIdentificacion());
		boolean isMatcher = matcher.matches();
		if(!isMatcher)
			return "El número de identificación debe contener solo letras y números";
		
		EmpleadoDTO empleadoFindDTO = empleadoEntityImp.findByNumeroIdentificacion(empleadoDTO.getNumeroIdentificacion());
		if(empleadoFindDTO != null)
			return "Ya existe un empleado creado con el mismo número de identificación. Por favor verifique la información.";
		String dominio = "";
		if(empleadoDTO.getPais().equalsIgnoreCase("Colombia"))
			dominio = "cidenet.com.co";
		else if (empleadoDTO.getPais().equalsIgnoreCase("Estados Unidos")) 
			dominio = "cidenet.com.us";
			
		String correoElectronico = "";
		String correoElectronicoStart = empleadoDTO.getPrimerNombre() + "." +empleadoDTO.getPrimerApellido();
		List<EmpleadoDTO> correosFind = empleadoEntityImp.findByCorreoElectronico(correoElectronicoStart);
		
		if(correosFind == null || correosFind.isEmpty()) {
			correoElectronico = empleadoDTO.getPrimerNombre() + "." +empleadoDTO.getPrimerApellido() + "@" + dominio;
		}else {
			int id = 1;
			if(correosFind.size() > 1) {
				id = correosFind.size() + 1;
			}
			correoElectronico = empleadoDTO.getPrimerNombre() + "." +empleadoDTO.getPrimerApellido() + "." + id + "@" + dominio;
		}
		
		if(correoElectronico.length() <= 300 )
		    empleadoDTO.setCorreoElectronico(correoElectronico);
		else
			return "No se pudo generar el correo electronico, ha superado el maximo de caracteres peritido.";
		
		empleadoDTO.setEstado("1");
		try {
			empleadoEntityImp.add(empleadoDTO);
			response = "Se ha añadido correctamente el empleado";
		}catch (Exception e) {
			response = e.getMessage();
		}
		
		return response;
	}
	
	private String quitarTildes(String nombre) {
		String cadenaNormalize = Normalizer.normalize(nombre, Normalizer.Form.NFD);   
		String cadenaSinAcentos = cadenaNormalize.replaceAll("[^\\p{ASCII}]", "");
		cadenaSinAcentos = cadenaSinAcentos.replaceAll("\"", "");
		return cadenaSinAcentos;
	}
	
	private String validarNombre(String campo, String nombreCampo, int maxLongitud) {
		String respuesta = "";
		
		Pattern  pattern = Pattern.compile("^[A-Z]*$");
		Matcher matcher = pattern.matcher(campo);
		boolean isMatcher = matcher.matches();
		
		if(!isMatcher) {
			respuesta =  "Recuerde que solo se permite caracteres de la A-Z";
		}
		
		if(campo.isEmpty() || campo.equals(null)) {
			respuesta =  "Debe ingresar el "+ nombreCampo + " del empleado";
		}
		
		if(campo.length() > maxLongitud) {
			respuesta = "El "+ nombreCampo + " no debe ser mayor de " + maxLongitud +" caracteres";
		}
		
		return respuesta;
	}
}
